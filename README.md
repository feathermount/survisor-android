## Survisor - a recommendation system for airbnb housings in NYC ##
### A class project for COMSE6998_009 Cloud Computing & Big Data.  
### Page Description (and corresponding classes)
1. Page1(MainActivity) contains the intro page of the app 
2. Page2(MainSearchActivity) contains the search page of the app. It contains buttons and seekbars, and their corresponding handlers. The input of the page will be transferred to the 3rd page for AirBnB queries. User can select 1) Move in date 2) duration of the stay 3) preference of food 4) preference of living qualities  
3. Page3(DisplayMessageActivity) contains the page the list the airbnb housings recommended by our system. It communicates with our web server through HTTP protocol, sending/receiving the data in JSON format. And shows the query result on a custom ListView.  
4. Page4(DetailActivity) contains the information of a housing detail from Airbnb. It will get launched if user clicks on a specific items listed in the ListView in page 3.  
  
### Version 
1.0  

### How do I get set up? 
1. You need to install the latest Android Studio [[link](https://developer.android.com/sdk/index.html)]  
2. git clone the project source, then open Android Studio -> File -> Open -> Select for the folder you just cloned  
3. choose run, and launch the app in the emulator  

### Dependencies 
Android SDK 22.0.1  
Android Volley Library (for HTTP communication over JSON)  

###  Database configuration 
We host our database on mongodb. We created 3 different tables in the database: AirBnB, Yelp and NYC311, the description of the corresponding columns in each table can be found here:  
[https://github.com/FeatherMount/Survisor](https://github.com/FeatherMount/Survisor)  

###  Deployment instructions
We have deployed our web server (based on python flask) in heroku. We use git base methodology to deploy the server. The source code of our web server can be found at here:  
[https://github.com/FeatherMount/Survisor](https://github.com/FeatherMount/Survisor)  

### Contribution guidelines   
Zhou Zhou, Shih-Wei Li, Jiayi Yan