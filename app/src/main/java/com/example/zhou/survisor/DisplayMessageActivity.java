package com.example.zhou.survisor;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.ExecutionException;

import static android.widget.AdapterView.*;


public class DisplayMessageActivity extends ActionBarActivity implements Response.Listener,
        Response.ErrorListener{

    private ListView listView;
    private static final String REQUEST_TAG = "DISPLAY_MESSAGE_ACTIVITY";
    private RequestQueue mQueue;
    private Hashtable<String, String> imgUrl;
    private Hashtable<String, String> itemDescription;
    private Hashtable<String, String> itemNeighborhood;
    private ArrayList<String> items;
    String[] descriptionText;
    String[] imgUrlView;
    String[] neighborhoodText;


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("error", error.getMessage());
    }

    @Override
    public void onResponse(Object response) {
        try {
            JSONObject JSONObj = (JSONObject) response;
            String id = JSONObj.getString("id");
            String description = JSONObj.getString("desc");
            String neighborhood = JSONObj.getString("neighborhood");
            String url = JSONObj.getString("photo");
            itemDescription.put(id, description);
            imgUrl.put(id, url);
            itemNeighborhood.put(id, neighborhood);
            for (int i = 0; i < items.size(); i++) {
                //Log.d("desc", itemDescription.get(items.get(i)));
                if (itemDescription.get(items.get(i)) == null)
                    continue;
                descriptionText[i] = itemDescription.get(items.get(i));
                imgUrlView[i] = imgUrl.get(items.get(i));
                neighborhoodText[i] = itemNeighborhood.get(items.get(i));
            }
            CustomListAdapter adapter = new CustomListAdapter(this, neighborhoodText, imgUrlView, descriptionText);
            listView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class RequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Do anything with response..
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        final Intent intent = getIntent();

        int year = Integer.parseInt(intent.getStringExtra(MainSearchActivity.YEAR_MESSAGE));
        int month = Integer.parseInt(intent.getStringExtra(MainSearchActivity.MONTH_MESSAGE));
        int day = Integer.parseInt(intent.getStringExtra(MainSearchActivity.DAY_MESSAGE));
        int duration = Integer.parseInt(intent.getStringExtra(MainSearchActivity.DURATION_MESSAGE));
        int food = Integer.parseInt(intent.getStringExtra(MainSearchActivity.FOOD_MESSAGE));
        int quality = Integer.parseInt(intent.getStringExtra(MainSearchActivity.QUALITY_MESSAGE));

        imgUrl = new Hashtable<>();
        itemDescription = new Hashtable<>();
        itemNeighborhood = new Hashtable<>();

        String uri = "https://obscure-inlet-8237.herokuapp.com/?food="+food+"&quality="+quality;
        AsyncTask<String, String, String> task = new RequestTask();
        task.execute(uri);

        items = new ArrayList<>();

        try {
            String responseString = task.get();
            JSONObject reader = new JSONObject(responseString);
            JSONArray ja = reader.getJSONArray("housing");
            for (int i = 0; i < ja.length(); i++) {
                items.add(((JSONObject) ja.get(i)).getString("data-id"));
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
        try {
            for (int i = 0; i < items.size(); i++){
                String url = "https://blooming-dawn-3299.herokuapp.com/messages";
                String json = "{\"id\":\"" + items.get(i) + "\",\"yr\":\"" + year +
                        "\", \"month\":\"" + month +
                        "\", \"day\":\"" + day +
                        "\", \"duration\":\"" + duration + "\"}";

                //Log.d("POST: ", json);

                JSONObject jsonObj = new JSONObject(json);

                CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method
                        .POST, url,
                        jsonObj, this, this);
                jsonRequest.setTag(REQUEST_TAG);
                mQueue.add(jsonRequest);
            }
        } catch (Exception e) {
            //Log.d("InputStream", e.getLocalizedMessage());
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        listView = (ListView) findViewById(R.id.listView);
        final Intent intentToDetail = new Intent(this, DetailActivity.class);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d("onItemClick: ", "touched");
                intentToDetail.putExtra("desc", itemDescription.get(items.get(position)));
                intentToDetail.putExtra("img", imgUrl.get(items.get(position)));
                intentToDetail.putExtra("id", items.get(position));
                startActivity(intentToDetail);
            }
        });

        descriptionText = new String[items.size()];
        imgUrlView = new String[items.size()];
        neighborhoodText = new String[items.size()];

        for (int i = 0; i < items.size(); i++) {
            //Log.d("desc", itemDescription.get(items.get(i)));
            descriptionText[i] = "This means that the host has changed their mind or the item is booked.";
            imgUrlView[i] = null;
            neighborhoodText[i] = "Not available!";
        }
        CustomListAdapter adapter = new CustomListAdapter(this, neighborhoodText, imgUrlView, descriptionText);
        listView.setAdapter(adapter);
        listView.setFocusable(false);
        listView.setFocusableInTouchMode(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void launchVolley(View view) {
        Intent mainVolleyActivity = new Intent(this, MainVolleyActivity.class);
        startActivity(mainVolleyActivity);
    }
}
