package com.example.zhou.survisor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;


public class NumberPickerFragment extends DialogFragment implements NumberPicker.OnValueChangeListener{
    public Context context;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // get context
        context = getActivity().getApplicationContext();
        // make dialog object
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // get the layout inflater
        LayoutInflater li = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // inflate our custom layout for the dialog to a View
        View view = li.inflate(R.layout.layout_number_picker, null);
        // inform the dialog it has a custom View
        builder.setView(view);
        // and if you need to call some method of the class
        NumberPicker np = (NumberPicker) view
                .findViewById(R.id.number_picker);
        np.setValue(1);
        np.setMaxValue(30);
        np.setOnValueChangedListener(this);
        // create the dialog from the builder then show
        return builder.create();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        ((MainSearchActivity)getActivity()).setDuration(newVal);
        ((Button)getActivity().findViewById(R.id.duration_button)).setText(Integer.toString(newVal));
    }
}
