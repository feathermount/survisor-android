package com.example.zhou.survisor;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.SeekBar;

import java.util.Date;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
<<<<<<< HEAD
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        intent.putExtra(YEAR_MESSAGE, Integer.toString(year));
        intent.putExtra(MONTH_MESSAGE, Integer.toString(month));
        intent.putExtra(DAY_MESSAGE, Integer.toString(day));
        intent.putExtra(DURATION_MESSAGE, Integer.toString(duration));

        SeekBar sbFood = (SeekBar)findViewById(R.id.seekBarFood);
        SeekBar sbQuality = (SeekBar)findViewById(R.id.seekBarQuality);

        intent.putExtra(FOOD_MESSAGE, Integer.toString(sbFood.getProgress()));
        intent.putExtra(QUALITY_MESSAGE, Integer.toString(sbQuality.getProgress()));

        startActivity(intent);
    }

    public void launchDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "Pick a check-in date");
    }

    public void launchDurationPicker(View view) {
        NumberPickerFragment newFragment = new NumberPickerFragment();
        newFragment.show(getFragmentManager(), "How long will you stay");
    }

    public void setDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
=======
    }*/

    public void launchMainSearch(View view) {
        Intent mainSearchActivity = new Intent(this, MainSearchActivity.class);
        startActivity(mainSearchActivity);
    }
}
