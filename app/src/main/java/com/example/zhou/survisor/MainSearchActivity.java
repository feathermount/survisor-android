package com.example.zhou.survisor;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.SeekBar;

import java.util.Date;


public class MainSearchActivity extends ActionBarActivity {

    public final static String YEAR_MESSAGE = "com.example.zhou.survisor.YEAR";
    public final static String MONTH_MESSAGE = "com.example.zhou.survisor.MONTH";
    public final static String DAY_MESSAGE = "com.example.zhou.survisor.DAY";
    public final static String DURATION_MESSAGE = "com.example.zhou.survisor.DURATION";
    public final static String FOOD_MESSAGE = "com.example.zhou.survisor.FOOD";
    public final static String QUALITY_MESSAGE = "com.example.zhou.survisor.QUALITY";

    private int year = 0;
    private int month = 0;
    private int day = 0;
    private int duration = 0;
    private int food = 0;
    private int quality = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        intent.putExtra(YEAR_MESSAGE, Integer.toString(year));
        intent.putExtra(MONTH_MESSAGE, Integer.toString(month));
        intent.putExtra(DAY_MESSAGE, Integer.toString(day));
        intent.putExtra(DURATION_MESSAGE, Integer.toString(duration));

        SeekBar sbFood = (SeekBar)findViewById(R.id.seekBarFood);
        SeekBar sbQuality = (SeekBar)findViewById(R.id.seekBarQuality);

        intent.putExtra(FOOD_MESSAGE, Integer.toString(sbFood.getProgress()));
        intent.putExtra(QUALITY_MESSAGE, Integer.toString(sbQuality.getProgress()));

        startActivity(intent);
    }

    public void launchDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "Pick a check-in date");
    }

    public void launchDurationPicker(View view) {
        NumberPickerFragment newFragment = new NumberPickerFragment();
        newFragment.show(getFragmentManager(), "How long will you stay");
    }

    public void setDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}